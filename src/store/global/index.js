const state = {
    token: '',
    roles: [],
};

const mutations = {
    SET_ROLES: (state, roles) => (state.roles = roles),
};

const actions = {
    setRoles({ commit }, roles) {
        commit('SET_ROLES', roles);
    },
};

export default {
    state,
    mutations,
    actions,
};
