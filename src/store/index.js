import { createStore } from 'vuex';
import global from './global';

const store_modules = import.meta.globEager('../views/*/store.js');
const modules = {};

for (const path in store_modules) {
    const key = /(?<=views\/)(\w*)(?=\/store)/.exec(path)[0];
    modules[key] = store_modules[path].default;
}

const store = createStore({
    ...global,
    modules,
});

export default store;
