import { createApp } from 'vue';
import axios from 'axios';
import App from './App.vue';
import router from './router';
import store from './store';
import Antd, { message, Modal } from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import initMocks from './mock';
import './config/interceptors';

const app = createApp(App);

app.use(store).use(router).use(Antd).mount('#app');

app.config.globalProperties.$message = message;
app.config.globalProperties.$Modal = Modal;
app.config.globalProperties.$axios = axios;

// mock为demo数据，移除mock注释掉initMocks
initMocks();
