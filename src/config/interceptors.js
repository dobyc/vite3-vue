import { message } from 'ant-design-vue';
import axios from 'axios';
import router from '../router';

axios.interceptors.request.use(
    function (config) {
        const token = sessionStorage.getItem('token');
        if (token) config.headers.token = token;
        return config;
    },
    function (error) {
        return Promise.reject(error);
    }
);

axios.interceptors.response.use(
    function (response) {
        console.log(`response -- `, response);

        if (response.status === 200) {
            const { data } = response;
            // 登录超时 / 没有登录
            if (data.head.errorCode === '10000') {
                router.push('/login');
            } else if (data.head.errorCode !== '0') {
                message.error(data.head.message);
            }
            return data;
        }

        return response;
    },
    function (error) {
        return Promise.reject(error);
    }
);
