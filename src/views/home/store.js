const state = {
    name: 'home',
};

const mutations = {};

const actions = {};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
};
