/**
 * 模拟数据
 */
let id = 3;
let deviceId = 3;

let tableData = [
    {
        id: '1',
        name: 'John Brown',
        age: 32,
        address: 'New York No. 1 Lake Park',
    },
    {
        id: '2',
        name: 'Jim Green',
        age: 42,
        address: 'London No. 1 Lake Park',
    },
    {
        id: '3',
        name: 'Joe Black',
        age: 32,
        address: 'Sidney No. 1 Lake Park',
    },
];

let deviceTableData = [
    {
        id: '1',
        name: '设备1',
        mac: '32:AE:2A:3E:D1:11',
        address: 'New York No. 1 Lake Park',
    },
    {
        id: '2',
        name: '设备2',
        mac: '12:AE:2A:3E:D1:11',
        address: 'London No. 2 Lake Park',
    },
    {
        id: '3',
        name: '设备3',
        mac: '52:AE:2A:3E:D1:11',
        address: 'Sidney No. 3 Lake Park',
    },
];

/**
 * 把url中的参数转为对象
 * @param {string} url 
 * @returns Object
 */
const getParams = (url) => {
    let params = {};
    const index = url.indexOf('?');
    if (index !== -1) {
        const paramsStr = url.slice(index + 1);
        paramsStr.split('&').forEach(item => {
            const _arr = item.split('=');
            params[_arr[0]] = _arr?.[1] || '';
        });
    }
    return params;
}

export default [
    {
        /**
         * 页面初始化数据，进入页面自动调用 /api/[路由]  
         * method: get
         */
        url: '/api/demo',
        type: 'get',
        response: (req, res, next) => {
            return {
                /**
                 * errorCode 错误码 0 代表无错误 *必须*
                 * errorMsg 错误信息 非必须
                 * requestId 随机字符串 用于日志查询 非必须
                 * timestamp 请求时间 用于日志查询 非必须
                 * time 后台代码执行时间 用于日志查询 非必须
                 * gtime 接口总花费时间 用于日志查询 非必须
                 */
                head: {
                    errorCode: '0',
                    errorMsg: '',
                    requestId: '3adb59fce3ea547b913333e7a58329be',
                    timestamp: 1588930172024,
                    time: 29,
                    gtime: 68,
                },
                body: {
                    // header 模块初始化
                    header: {
                        /** 
                         * 新增 / 编辑 弹出框初始化数据
                         * 组件使用antdv
                         * 参数含义请参考 https://2x.antdv.com/components/overview-cn
                         */
                        add: {
                            fields: [
                                // --------- 示例 -------------
                                // {
                                //     component: 'input',
                                //     key: 'age',
                                //     name: 'age',
                                //     value: '',
                                //     required: true,
                                //     allowClear: true,
                                //     label: '输入框',
                                //     placeholder: '请输入xxx',
                                //     rules: [
                                //         { required: true, message: '请输入xxx' },
                                //         { whitespace: true, message: '不能输入空格' },
                                //     ],
                                // },
                                // {
                                //     component: 'inputNumber',
                                //     key: 'inputNumber',
                                //     name: 'inputNumber',
                                //     placeholder: '请输入xxx',
                                //     label: 'inputNumber',
                                //     value: '',
                                //     required: true,
                                //     rules: [{ required: true, message: '请输入xxx' },],
                                // },
                                // {
                                //     component: 'rangePicker',
                                //     key: 'times',
                                //     name: 'times',
                                //     value: [],
                                //     allowClear: true,
                                //     label: 'rangePicker',
                                //     valueFormat: 'YYYY-MM-DD',
                                //     // placeholder: ['开始日期','结束日期'],
                                //     required: true,
                                // },
                                // {
                                //     component: 'datePicker',
                                //     key: 'date',
                                //     name: 'date',
                                //     value: '',
                                //     allowClear: true,
                                //     label: 'datePicker',
                                //     valueFormat: 'YYYY-MM-DD',
                                //     placeholder: '请选择xxx',
                                //     required: true,
                                // },
                                // {
                                //     component: 'monthPicker',
                                //     key: 'monthPicker',
                                //     name: 'monthPicker',
                                //     value: '',
                                //     allowClear: true,
                                //     label: 'monthPicker',
                                //     valueFormat: 'YYYY-MM',
                                //     required: true,
                                //     placeholder: '请选择xxx',
                                // },
                                // {
                                //     component: 'weekPicker',
                                //     key: 'weekPicker',
                                //     name: 'weekPicker',
                                //     value: '',
                                //     allowClear: true,
                                //     label: 'weekPicker',
                                //     placeholder: '请选择xxx',
                                //     required: true,
                                // },
                                // {
                                //     component: 'timePicker',
                                //     key: 'time',
                                //     name: 'time',
                                //     value: '',
                                //     allowClear: true,
                                //     label: 'timePicker',
                                //     valueFormat: 'YYYY-MM-DD',
                                //     required: true,
                                // },
                                // {
                                //     component: 'select',
                                //     key: 'select',
                                //     name: 'select',
                                //     value: undefined,
                                //     allowClear: true,
                                //     label: '下拉框',
                                //     placeholder: '请选择xxx',
                                //     showSearch: true,
                                //     required: true,
                                //     options: [
                                //         {
                                //             value: 1,
                                //             label: 'select 1',
                                //             disabled: true,
                                //         },
                                //         {
                                //             value: 2,
                                //             label: 'select 2',
                                //         },
                                //     ],
                                // },
                                // {
                                //     component: 'radioGroup',
                                //     label: 'radioGroup',
                                //     key: 'radioGroup',
                                //     name: 'radioGroup',
                                //     required: true,
                                //     options: [
                                //         { label: 'Apple', value: 'Apple' },
                                //         { label: 'Pear', value: 'Pear' },
                                //         { label: 'Orange', value: 'Orange' },
                                //     ]
                                // },
                                // {
                                //     component: 'checkboxGroup',
                                //     label: 'checkboxGroup',
                                //     key: 'checkboxGroup',
                                //     name: 'checkboxGroup',
                                //     required: true,
                                //     options: [
                                //         { label: 'Apple', value: 'Apple' },
                                //         { label: 'Pear', value: 'Pear' },
                                //         { label: 'Orange', value: 'Orange' },
                                //     ]
                                // },
                                // {
                                //     component: 'switch',
                                //     label: 'switch',
                                //     key: 'switch',
                                //     name: 'switch',
                                //     required: true,
                                // },
                                // {
                                //     component: 'rate',
                                //     label: 'rate',
                                //     key: 'rate',
                                //     name: 'rate',
                                //     required: true,
                                // },
                                // {
                                //     component: 'slider',
                                //     label: 'slider',
                                //     key: 'slider',
                                //     name: 'slider',
                                //     required: true,
                                // },
                                {
                                    component: 'input',
                                    key: 'name',
                                    name: 'name',
                                    value: '',
                                    required: true,
                                    allowClear: true,
                                    label: '姓名',
                                    placeholder: '请输入姓名',
                                    rules: [{ required: true, message: '请输入姓名' },],
                                },
                                {
                                    component: 'inputNumber',
                                    key: 'age',
                                    name: 'age',
                                    placeholder: '请输入年龄',
                                    label: '年龄',
                                    value: '',
                                    precision: 0,
                                    required: true,
                                    rules: [{ required: true, message: '请输入年龄' },],
                                },
                                {
                                    component: 'input',
                                    key: 'address',
                                    name: 'address',
                                    value: '',
                                    required: true,
                                    allowClear: true,
                                    label: '地址',
                                    placeholder: '请输入地址',
                                    rules: [{ required: true, message: '请输入地址' },],
                                },
                            ],
                        },
                        /**
                         * 搜索条件初始化数据
                         * 参数同add
                         */
                        search: {
                            fields: [
                                {
                                    component: 'input',
                                    name: 'keyword',
                                    value: '',
                                    allowClear: true,
                                    label: '输入框',
                                    placeholder: '请输入你要搜索的姓名',
                                },
                            ],
                        },
                        /**
                         * 搜索区显示按钮 
                         * add 新增按钮
                         * search 搜索按钮
                         */
                        buttons: ['add', 'search'],
                    },
                    /**
                     * table 区域
                     */
                    content: {
                        /** 
                         * 表格列 
                         * 参考https://2x.antdv.com/components/table-cn#Column
                         */
                        columns: [
                            {
                                title: '姓名',
                                dataIndex: 'name',
                                align: 'center',
                                slots: { title: 'customTitle', customRender: 'name' },
                            },
                            {
                                title: '年龄',
                                dataIndex: 'age',
                                align: 'center',
                            },
                            {
                                title: '地址',
                                dataIndex: 'address',
                                align: 'center',
                            },
                            {
                                title: '操作',
                                dataIndex: 'action',  // 操作列固定action
                                slots: { customRender: 'action' },
                                align: 'center',
                                actions: [
                                    {
                                        action: 'edit',
                                        type: 'link',
                                        text: '编辑'
                                    },
                                    {
                                        action: 'delete',
                                        type: 'link',
                                        text: '删除'
                                    },
                                ]
                            },
                        ],
                        /**
                         * 表格分页
                         * current 当前页码 默认值 1
                         * pageSize 每页条数 默认值 10
                         */
                        pager: {},
                        /**
                         * 组件接受参数 
                         * 参考https://2x.antdv.com/components/table-cn#API
                         * rowKey 指定唯一key值 *必须*
                         */
                        options: {
                            rowKey: 'id'
                        }
                    },
                    /**
                     * 预留 暂无
                     */
                    footer: {},
                },
            };
        },
    },
    {
        /**
         * 
         */
        url: /\/api\/demo\/table\w*/,
        type: 'get',
        response: (req, res, next) => {
            const params = getParams(req.url);
            const i = ~~params.current - 1;
            const datas = tableData.filter(item => item.name.indexOf(params.keyword) !== -1);

            return {
                head: {
                    requestId: '3adb59fce3ea547b913333e7a58329be',
                    errorCode: '0',
                    timestamp: 1588930172024,
                    time: 29,
                    gtime: 68,
                },
                body: {
                    datas: datas.slice(i * params.pageSize, params.current * params.pageSize - 1),
                    pager: {
                        current: ~~params.current || 1,
                        pageSize: ~~params.pageSize || 10,
                        total: datas.length,
                    },
                },
            };
        },
    },
    {
        url: '/api/demo',
        type: 'put',
        response: (req, res, next) => {
            const data = JSON.parse(req.body);
            tableData.push({
                id: ++id,
                name: data.name,
                age: data.age,
                address: data.address
            });

            return {
                head: {
                    requestId: '3adb59fce3ea547b913333e7a58329be',
                    errorCode: '0',
                    message: '新增成功',
                    timestamp: 1588930172024,
                    time: 29,
                    gtime: 68,
                },
            };
        },
    },
    {
        url: '/api/demo',
        type: 'post',
        response: (req, res, next) => {
            const data = JSON.parse(req.body);
            if (!data.id) {
                return {
                    head: {
                        requestId: '3adb59fce3ea547b913333e7a58329be',
                        errorCode: '1',
                        message: 'id不能为空',
                        timestamp: 1588930172024,
                        time: 29,
                        gtime: 68,
                    },
                };
            } else {
                tableData = tableData.map(item => item.id != data.id ? item : ({
                    ...item,
                    name: data.name,
                    age: data.age,
                    address: data.address
                }));

                return {
                    head: {
                        requestId: '3adb59fce3ea547b913333e7a58329be',
                        errorCode: '0',
                        message: '编辑成功',
                        timestamp: 1588930172024,
                        time: 29,
                        gtime: 68,
                    },
                };
            }
        },
    },
    {
        url: /\/api\/demo\w*/,
        type: 'delete',
        response: (req, res, next) => {
            const params = getParams(req.url);
            if (!params.id) {
                return {
                    head: {
                        requestId: '3adb59fce3ea547b913333e7a58329be',
                        errorCode: '1',
                        message: 'id不能为空',
                        timestamp: 1588930172024,
                        time: 29,
                        gtime: 68,
                    },
                };
            } else {
                tableData = tableData.filter(item => item.id != params.id);

                return {
                    head: {
                        requestId: '3adb59fce3ea547b913333e7a58329be',
                        errorCode: '0',
                        message: '删除成功',
                        timestamp: 1588930172024,
                        time: 29,
                        gtime: 68,
                    },
                };
            }


        },
    },

    {
        url: '/api/device',
        type: 'get',
        response: (req, res, next) => {
            return {
                head: {
                    requestId: '3adb59fce3ea547b913333e7a58329be',
                    errorCode: '0',
                    timestamp: 1588930172024,
                    time: 29,
                    gtime: 68,
                },
                body: {
                    header: {
                        add: {
                            fields: [
                                {
                                    component: 'input',
                                    key: 'name',
                                    name: 'name',
                                    value: '',
                                    required: true,
                                    allowClear: true,
                                    label: '设备名',
                                    placeholder: '请输入设备名',
                                    rules: [{ required: true, message: '请输入设备名' },],
                                },
                                {
                                    component: 'input',
                                    key: 'mac',
                                    name: 'mac',
                                    placeholder: '请输人mac地址',
                                    allowClear: true,
                                    label: 'mac',
                                    value: '',
                                    precision: 0,
                                    required: true,
                                    rules: [{ required: true, message: '请输入mac地址' },],
                                },
                                {
                                    component: 'input',
                                    key: 'address',
                                    name: 'address',
                                    value: '',
                                    required: true,
                                    allowClear: true,
                                    label: '地址',
                                    placeholder: '请输入地址',
                                    rules: [{ required: true, message: '请输入地址' },],
                                },
                            ],
                        },
                        search: {
                            fields: [
                                {
                                    component: 'input',
                                    name: 'keyword',
                                    value: '',
                                    allowClear: true,
                                    label: '输入框',
                                    placeholder: '请输入你要搜索的设备名',
                                },
                            ],
                        },
                        buttons: ['add', 'search'],
                    },
                    content: {
                        columns: [
                            {
                                title: '设备名',
                                dataIndex: 'name',
                                align: 'center',
                                slots: { title: 'customTitle', customRender: 'name' },
                            },
                            {
                                title: 'mac',
                                dataIndex: 'mac',
                                align: 'center',
                            },
                            {
                                title: '地址',
                                dataIndex: 'address',
                                align: 'center',
                            },
                            {
                                title: '操作',
                                dataIndex: 'action',  // 操作列固定action
                                slots: { customRender: 'action' },
                                align: 'center',
                                actions: [
                                    {
                                        action: 'edit',
                                        type: 'link',
                                        text: '编辑'
                                    },
                                    {
                                        action: 'delete',
                                        type: 'link',
                                        text: '删除'
                                    },
                                ]
                            },
                        ],
                        pager: {},
                        options: {
                            rowKey: 'id'
                        }
                    },
                    footer: {},
                },
            };
        },
    },
    {
        url: /\/api\/device\/table\w*/,
        type: 'get',
        response: (req, res, next) => {
            const params = getParams(req.url);
            const i = ~~params.current - 1;
            const datas = deviceTableData.filter(item => item.name.indexOf(params.keyword) !== -1);

            return {
                head: {
                    requestId: '3adb59fce3ea547b913333e7a58329be',
                    errorCode: '0',
                    timestamp: 1588930172024,
                    time: 29,
                    gtime: 68,
                },
                body: {
                    datas: datas.slice(i * params.pageSize, params.current * params.pageSize - 1),
                    pager: {
                        current: ~~params.current || 1,
                        pageSize: ~~params.pageSize || 10,
                        total: datas.length,
                    },
                },
            };
        },
    },
    {
        url: '/api/device',
        type: 'put',
        response: (req, res, next) => {
            const data = JSON.parse(req.body);
            deviceTableData.push({
                id: ++deviceId,
                name: data.name,
                mac: data.mac,
                address: data.address
            });

            return {
                head: {
                    requestId: '3adb59fce3ea547b913333e7a58329be',
                    errorCode: '0',
                    message: '新增成功',
                    timestamp: 1588930172024,
                    time: 29,
                    gtime: 68,
                },
            };
        },
    },
    {
        url: '/api/device',
        type: 'post',
        response: (req, res, next) => {
            const data = JSON.parse(req.body);
            if (!data.deviceId) {
                return {
                    head: {
                        requestId: '3adb59fce3ea547b913333e7a58329be',
                        errorCode: '1',
                        message: 'id不能为空',
                        timestamp: 1588930172024,
                        time: 29,
                        gtime: 68,
                    },
                };
            } else {
                deviceTableData = deviceTableData.map(item => item.id != data.id ? item : ({
                    ...item,
                    name: data.name,
                    mac: data.mac,
                    address: data.address
                }));

                return {
                    head: {
                        requestId: '3adb59fce3ea547b913333e7a58329be',
                        errorCode: '0',
                        message: '编辑成功',
                        timestamp: 1588930172024,
                        time: 29,
                        gtime: 68,
                    },
                };
            }
        },
    },
    {
        url: /\/api\/device\w*/,
        type: 'delete',
        response: (req, res, next) => {
            const params = getParams(req.url);
            if (!params.id) {
                return {
                    head: {
                        requestId: '3adb59fce3ea547b913333e7a58329be',
                        errorCode: '1',
                        message: 'id不能为空',
                        timestamp: 1588930172024,
                        time: 29,
                        gtime: 68,
                    },
                };
            } else {
                deviceTableData = deviceTableData.filter(item => item.id != params.id);

                return {
                    head: {
                        requestId: '3adb59fce3ea547b913333e7a58329be',
                        errorCode: '0',
                        message: '删除成功',
                        timestamp: 1588930172024,
                        time: 29,
                        gtime: 68,
                    },
                };
            }


        },
    },
];
