import { createRouter, createWebHashHistory } from 'vue-router';
import store from '../store';
import axios from 'axios';
import Layout from '../layout/Layout.vue';

const modules = import.meta.glob('../views/*/*.vue');

const routes = [
    {
        path: '/',
        name: 'Layout',
        redirect: '/home',
        component: Layout,
        children: [
            {
                path: '/home',
                component: modules[`../views/home/Home.vue`],
                // meta: { title: '首页', icon: 'dashboard', affix: true },
            },
        ],
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import(/* webpackChunkName: "login" */ '../views/login/Login.vue'),
    },
];

const router = createRouter({
    history: createWebHashHistory(),
    routes,
});

router.beforeEach(async (to, from, next) => {
    const token = localStorage.getItem('token');
    if (store.state.roles.length === 0 && token) {
        const res = await axios.get('/api/roles');
        if (res.head.errorCode === '0') {
            store.dispatch('setRoles', res.body || []);
            res.body.forEach(async item => {
                if (item.functionType === 'menu') {
                    router.addRoute('Layout', {
                        path: '/' + item.path,
                        component: modules[`../views/${item.project_path}.vue`]
                    });
                }else if(item.functionType === 'template'){
                    router.addRoute('Layout', {
                        path: '/' + item.path,
                        component: modules[`../views/template/Template.vue`]
                    });
                }
            });
            next({ ...to, replace: true });
        }
    } else {
        next();
    }
});

export default router;
