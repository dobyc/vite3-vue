/**
 * 权限菜单示例
 * title 页面标题
 * name 菜单文本
 * path 访问路径
 * functionType [menu自定义页面 | template约定式页面]
 * project_path 自定义页面在views下的路径
 * imageUrl 菜单图标
 */
export default [
    {
        url: '/api/roles',
        type: 'get',
        response: (req, res, next) => {
            return {
                /**
                 * errorCode 错误码 0 代表无错误 *必须*
                 * errorMsg 错误信息 非必须
                 * requestId 随机字符串 用于日志查询 非必须
                 * timestamp 请求时间 非必须
                 * time 后台代码执行时间 非必须
                 * gtime 接口总花费时间 非必须
                 */
                head: {
                    errorCode: '0',
                    errorMsg: '',
                    requestId: '3adb59fce3ea547b913333e7a58329be',
                    timestamp: 1588930172024,
                    time: 29,
                    gtime: 68,
                },
                body: [
                    {
                        title: '关于我们',
                        name: '关于我们',
                        path: 'about',
                        functionType: 'menu',
                        project_path: 'about/About',
                        imageUrl: 'about',
                    },
                    {
                        title: '模版',
                        name: '模版',
                        path: 'demo',
                        functionType: 'template',
                        imageUrl: 'device',
                    },
                    {
                        title: '模版',
                        name: '模版-设备',
                        path: 'device',
                        functionType: 'template',
                        imageUrl: 'device',
                    },
                ],
            };
        },
    },
];
