import Mock from 'Mockjs';
import global from './global';

export default function initMocks() {
    const modules = import.meta.globEager('../views/*/mock.js');
    const mocks = [...global];

    for (const path in modules) {
        Array.prototype.push.apply(mocks, modules[path].default);
    }

    mocks.map((item) => {
        Mock.mock(item.url, item.type, item.response);
    });
}
